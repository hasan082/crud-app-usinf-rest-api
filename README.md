# Flutter CRUD App with a REST API

This is a Flutter app that demonstrates basic CRUD (Create, Read, Update, Delete) operations using a RESTful API.

## Overview

The Flutter CRUD app interacts with a REST API to perform the following operations:

- Retrieve a list of users from the API and display them in a list view.
- Delete a user by sending a DELETE request to the API.
- Additional operations like creating and updating users can be implemented based on your requirements.

## Screenshot

<img src="https://gitlab.com/hasan082/crud-app-usinf-rest-api/-/raw/main/crudapp.png" alt="" width="300">


## Project Structure

The project follows a typical Flutter project structure:

- `lib/main.dart`: The entry point of the app.
- `lib/home_page.dart`: Implements the `HomePage` widget, which displays the list of users and handles user deletion.
- `pubspec.yaml`: Contains the app's dependencies.

## Getting Started

To run the app, follow these steps:

1. Ensure you have Flutter and Dart SDK installed.
2. Clone the repository.
3. Open the project in your preferred IDE or text editor.
4. Run `flutter pub get` to install the dependencies.
5. Connect a device or start an emulator.
6. Run `flutter run` to launch the app on the device/emulator.


## Customization

You can customize the app by modifying the following files:

- `lib/home_page.dart`: Modify the UI and logic to handle additional CRUD operations, such as creating and updating users.
- Update the REST API endpoints in the code according to your API's URL and endpoints.

Feel free to explore and extend the functionality of the app as per your requirements.

## Dependencies

The app uses the following dependencies:

- `http`: A package for making HTTP requests to the REST API.

Make sure to check the `pubspec.yaml` file for the exact version of the dependencies used in the project.

## REST API

The app interacts with a REST API to perform CRUD operations. The API should have endpoints for retrieving a list of users and deleting a user. Update the API endpoints in the code to match your API's specifications.

## License

This project is licensed under the [MIT License](https://opensource.org/license/mit/).
